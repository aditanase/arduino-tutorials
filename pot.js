var five = require("johnny-five"),
board = new five.Board(),
shiftRegister, myLed, pot;

board.on("ready", function() {
	shiftRegister = new five.ShiftRegister({
		pins: {
			data: 8,
			clock: 9,
			latch: 10
		}
	});

	this.repl.inject({
		sr: shiftRegister
	});

	myLed = new five.Led(3);
	this.repl.inject({
		led: myLed
	});

	
	pot = new five.Sensor({
		pin: "A0",
		freq: 250
	});

	pot.scale(0,8).on("data", function() {
		var i = Math.round(this.value),
		out = Math.pow(2, i) - 1;

		console.log( this.value, this.analog, i, out);

		shiftRegister.send(out);
	});

});