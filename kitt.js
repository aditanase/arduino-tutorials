var five = require("johnny-five"),
    board = new five.Board(),
    shiftRegister, myLed,
    kitt = [1,2,4,8,16,32,64,128,64,32,16,8,4,2];

board.on("ready", function() {
  shiftRegister = new five.ShiftRegister({
    pins: {
      data: 8,
      clock: 9,
      latch: 10
    }
  });

  var i = -1;

  function next() {
    i = (i + 1) % 14;
    shiftRegister.send(kitt[i]);
    setTimeout(next, 75); 
  }

  this.repl.inject({
    sr: shiftRegister
  });

  myLed = new five.Led(3);

  // myLed.strobe( 1000 );

  // make myLED available as "led" in REPL

  this.repl.inject({
      led: myLed
  });

  next();

});