var j5 = require("johnny-five"),
board = new j5.Board(),
photoresistor,
LEDPIN = 13,
OUTPUT = 1;

board.on("ready", function(){

    photoresistor = new j5.Sensor({
        pin: "A2",
        freq: 250
    });

    // Set pin 13 to OUTPUT mode
    this.pinMode(LEDPIN, OUTPUT);

    // "data" get the current reading from the photoresistor
    photoresistor.on("data", function() {
        console.log( this.value );
        board.digitalWrite(LEDPIN, (this.value < 500 ? 0 : 1));
    });

});