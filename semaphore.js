/* global require */

var five = require("johnny-five"),
	board = new five.Board(),
	shiftRegister,
	but1, but2, but3,
	gauge = [15, 63, 255],
	sem = [15, 48, 192];

board.on("ready", function() {

	// used to drive the 8 leds

	shiftRegister = new five.ShiftRegister({
		pins: {
			data: 8,
			clock: 9,
			latch: 10
		}
	});

	this.repl.inject({
		sr: shiftRegister
	});

	but1 = new five.Button({ pin: 2, invert: true });
	but2 = new five.Button({ pin: 3, invert: true });
	but3 = new five.Button({ pin: 4, invert: true });

	but1.on("down", setLeds(sem, 0));
	but2.on("down", setLeds(sem, 1));
	but3.on("down", setLeds(sem, 2));
});

function setLeds(data, i) {
	console.log(i, data[i]);

	return function () {
		shiftRegister.send(data[i]);
	}
}